from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import config
import time


options = Options()
options.add_argument('--headless')
options.add_argument('--disable-gpu')

chrome_path = 'chromedriver.exe'


driver = webdriver.Chrome(chrome_path, options=options)

driver.get('https://www.reddit.com/login')

username = driver.find_element_by_xpath('''//*[@id="loginUsername"]''').send_keys(config.username)
password = driver.find_element_by_xpath('''//*[@id="loginPassword"]''').send_keys(config.password)
fulllogin = driver.find_element_by_xpath('''/html/body/div/div/div[2]/div/form/fieldset[5]/button''').click()
print('Logged into reddit with user ' + config.username)
time.sleep(6)

for sub in config.list_of_subs:
 driver.get(sub)
 title = driver.find_element_by_xpath('''//*[@id="SHORTCUT_FOCUSABLE_DIV"]/div/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/div[2]/div[1]/textarea''').send_keys(config.posttitle)
 imageurl = driver.find_element_by_xpath('''//*[@id="SHORTCUT_FOCUSABLE_DIV"]/div/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/div[2]/div[2]/textarea''').send_keys(config.imageurl)
 clicksubmit = driver.find_element_by_xpath('''//*[@id="SHORTCUT_FOCUSABLE_DIV"]/div/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/div[3]/div[2]/div/div[1]/button''').click()
 time.sleep(2)
 print('Posted to ' + sub + ' at ' + driver.current_url)
 time.sleep(660)